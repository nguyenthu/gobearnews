package gobear.news;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import gobear.news.resource.EspressoIdlingResource;
import gobear.news.ui.activity.LoginActivity;
import gobear.news.ui.activity.NewsDetailActivity;
import gobear.news.ui.activity.NewsListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.hamcrest.core.AnyOf.anyOf;

/**
 * Created by Thu Nguyen on 3/8/2019.
 */
@RunWith(AndroidJUnit4.class)
public class NewsListActivityTest {

    @Rule
    public ActivityTestRule<NewsListActivity> activityRules = new ActivityTestRule<NewsListActivity>(NewsListActivity.class, true, true){
    };

    @Before
    public void setUp(){
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource());
    }
    @After
    public void tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource());
    }

    @Test
    public void testNoNewsListDueToNetwork(){
        onView(withId(R.id.rvNews)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }
    @Test
    public void testHasNewsListFromLocalStorageDespiteNoNetwork(){
        onView(withId(R.id.rvNews)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    /**
     * Click an item on recyler view then navigate to news detail screen
     */
    @Test
    public void testClickNewsItem(){
        onView(withId(R.id.rvNews))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Intents.init();
        intending(anyOf(hasComponent(NewsDetailActivity.class.getName())))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null));
        // inject User instance here
        intended(hasComponent(NewsDetailActivity.class.getName()), times(1));
        Intents.release();
    }
    @Test
    public void testLogOut(){
        Intents.init();
        intending(anyOf(hasComponent(LoginActivity.class.getName())))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null));
        // inject User instance here
        onView(withId(R.id.tvSkip)).perform(click());
        intended(hasComponent(LoginActivity.class.getName()), times(1));
        Intents.release();
    }

}
