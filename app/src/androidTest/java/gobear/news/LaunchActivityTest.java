package gobear.news;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import gobear.news.data.db.prefs.AppPreferencesHelper;
import gobear.news.ui.activity.LaunchActivity;
import gobear.news.ui.activity.LoginActivity;
import gobear.news.ui.activity.NewsListActivity;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.AnyOf.anyOf;

/**
 * Created by Thu Nguyen on 3/8/2019.
 */
@RunWith(AndroidJUnit4.class)
public class LaunchActivityTest {
    @Rule
    public ActivityTestRule<LaunchActivity> activityRules = new ActivityTestRule<LaunchActivity>(LaunchActivity.class, true, false){
    };

    @Before
    public void setUp(){
    }

    @Test
    public void testClickSkipButtonToFinish(){
        Intents.init();
        intending(anyOf(hasComponent(LoginActivity.class.getName())))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null));
        // inject User instance here
        activityRules.launchActivity(null);
        onView(withId(R.id.tvSkip)).perform(click());
        intended(hasComponent(LoginActivity.class.getName()), times(1));
        Intents.release();

    }


    /**
     * User clicked "Skip" before and user hasn't login yet.
     */
    @Test
    public void testNavigateToLoginScreenAutomatically(){

        Intents.init();
        intending(anyOf(hasComponent(LoginActivity.class.getName())))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null));
        // inject User instance here
        activityRules.launchActivity(null);
        intended(hasComponent(LoginActivity.class.getName()), times(1));
        Intents.release();
    }
    /**
     * User clicked "Skip" before and user already login yet.
     */
    @Test
    public void testNavigateToNewsListScreenAutomatically(){

        Intents.init();
        intending(anyOf(hasComponent(NewsListActivity.class.getName())))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_CANCELED, null));
        // inject User instance here
        activityRules.launchActivity(null);
        intended(hasComponent(NewsListActivity.class.getName()), times(1));
        Intents.release();
    }

}
