package gobear.news;

import android.app.Instrumentation;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import gobear.news.data.db.prefs.AppPreferencesHelper;
import gobear.news.ui.activity.LaunchActivity;
import gobear.news.ui.activity.LoginActivity;
import gobear.news.ui.activity.NewsListActivity;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Thu Nguyen on 3/8/2019.
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {
    String userName = "GoBear";
    String password = "GoBearDemo";

    @Rule
    public ActivityTestRule<LoginActivity> activityRules = new ActivityTestRule<LoginActivity>(LoginActivity.class, true, true){
    };

    @Before
    public void setUp(){
    }

    @Test
    public void testLoginFailedDueToUserName(){
        // register news list activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(NewsListActivity.class.getName(), null, false);

        onView(withId(R.id.etUserName)).perform(typeText("GoBearBear"));
        onView(withId(R.id.etPassword)).perform(typeText(password));
        onView(withId(R.id.btnLogin)).perform(click());


        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it's in seconds.
        NewsListActivity newsListActivity = (NewsListActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNull(newsListActivity);
    }
    @Test
    public void testLoginFailedDueToPassword(){
        // register news list activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(NewsListActivity.class.getName(), null, false);

        onView(withId(R.id.etUserName)).perform(typeText(userName));
        onView(withId(R.id.etPassword)).perform(typeText("GoBear"));
        onView(withId(R.id.btnLogin)).perform(click());
        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it's in seconds.
        NewsListActivity newsListActivity = (NewsListActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNull(newsListActivity);

    }
    @Test
    public void testLoginFailedDueToUsernameAndPassword(){
        // register news list activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(NewsListActivity.class.getName(), null, false);

        onView(withId(R.id.etUserName)).perform(typeText("GoBearBear"));
        onView(withId(R.id.etPassword)).perform(typeText("GoBear"));
        onView(withId(R.id.btnLogin)).perform(click());
        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it's in seconds.
        NewsListActivity newsListActivity = (NewsListActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNull(newsListActivity);

    }
    @Test
    public void testLoginSuccess(){
        // register news list activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(NewsListActivity.class.getName(), null, false);

        onView(withId(R.id.etUserName)).perform(typeText(userName));
        onView(withId(R.id.etPassword)).perform(typeText(password));
        onView(withId(R.id.btnLogin)).perform(click());
        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it's in seconds.
        NewsListActivity newsListActivity = (NewsListActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNotNull(newsListActivity);
        newsListActivity.finish();
    }
}
