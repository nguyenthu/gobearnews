package gobear.news;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import gobear.news.data.model.NewsItemData;
import gobear.news.ui.activity.LaunchActivity;
import gobear.news.ui.activity.LoginActivity;
import gobear.news.ui.activity.NewsDetailActivity;
import gobear.news.ui.activity.NewsListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.AnyOf.anyOf;

/**
 * Created by ThuNguyen on 3/10/19.
 */
@RunWith(AndroidJUnit4.class)
public class NewsDetailActivityTest {
    @Rule
    public ActivityTestRule<NewsListActivity> newsListRules = new ActivityTestRule<NewsListActivity>(NewsListActivity.class, true, true){
    };
    @Rule
    public ActivityTestRule<NewsDetailActivity> newsDetailRules = new ActivityTestRule<NewsDetailActivity>(NewsDetailActivity.class, true, false){
    };
    @Before
    public void setUp(){
    }
    @Test
    public void testClickBackButton(){
        // inject User instance here
        newsDetailRules.launchActivity(null);
        Espresso.pressBack();
        assertTrue(newsDetailRules.getActivity().isDestroyed());
    }
    @Test
    public void testIntent(){
        NewsItemData newsItemData = new NewsItemData();
        newsItemData.title = "Title";
        newsItemData.description = "Description";
        newsItemData.date = "Thu, 21 Feb 2019 08:05:15 GMT";
        newsItemData.thumbnail = new NewsItemData.Thumbnail();
        newsItemData.thumbnail.setUrl("http://c.files.bbci.co.uk/10C2B/production/_105715686_hi051546893.jpg");

        Intent intent = new Intent(newsListRules.getActivity(), NewsDetailActivity.class);
        intent.putExtra("news_item_data", newsItemData);
        newsDetailRules.launchActivity(intent);

        onView(withId(R.id.tvNewsStory)).check(matches(withText("Description")));
    }
}
