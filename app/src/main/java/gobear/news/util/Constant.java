package gobear.news.util;

/**
 * Created by ThuNguyen on 3/10/19.
 */

public class Constant {
    public static final String FROM_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss";
    public static final String TO_DATE_FORMAT = "dd/MM/yyyy";
}
