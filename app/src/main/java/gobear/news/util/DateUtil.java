package gobear.news.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Thu Nguyen on 3/9/19.
 */

public class DateUtil {
    public static String displayDateFormat(String fromFormat, String fromDate, String toFormat){
        try {
            DateFormat formatter = new SimpleDateFormat(fromFormat);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = (Date)formatter.parse(fromDate);
            return new SimpleDateFormat(toFormat, Locale.getDefault()).format(date);
        }
        catch (ParseException e)
        {
            System.out.println("Exception :"+e);
        }
        return null;
    }
}
