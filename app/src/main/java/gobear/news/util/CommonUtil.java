package gobear.news.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.DisplayMetrics;

/**
 * Created by ThuNguyen on 3/10/19.
 */

public class CommonUtil {
    public static int getStatusBarHeight(Context pContext) {
        Resources resources = pContext.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
    public static int getScreenHeight(Activity activity){
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics.heightPixels;
    }
    public static int getScreenHeightWithoutStatusBar(Activity activity){
        int screenHeight = getScreenHeight(activity);
        int statusBarHeight = getStatusBarHeight(activity);
        return (screenHeight - statusBarHeight);
    }
    public static int getScreenWidth(Activity activity){
        try{
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.widthPixels;
        }
        catch(Exception e){

        }
        return 0;

    }

    /** Calculates the Action Bar height in pixels. */
    public static int calculateActionBarSize(Context context) {
        if (context == null) {
            return 0;
        }

        Resources.Theme curTheme = context.getTheme();
        if (curTheme == null) {
            return 0;
        }

        TypedArray att = curTheme.obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        if (att == null) {
            return 0;
        }

        float size = att.getDimension(0, 0);
        att.recycle();
        return (int) size;
    }
}
