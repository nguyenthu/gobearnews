package gobear.news.ui.adapter.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import gobear.news.ui.viewholder.base.BaseViewHolder;

public abstract class BaseViewAdapter<T> extends RecyclerView.Adapter<BaseViewHolder>{
    protected Context context;
    public static final int VIEW_ITEM = 1;
    public static final int VIEW_PROG = 0;
    public static final int VIEW_HEADER = 2;
    public static final int VIEW_FOOTER = 3;
    List<T> dataList;

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

    }
    public T getItem(int position) {
        return dataList.get(position);
    }

    public void setDataList(List<T> dataList){
        this.dataList = dataList;
    }

    public List<T> getDataList(){
        return dataList;
    }

    /**
     * Add the items to the current list on last
     * @param dataList
     */
    public void addItemsOnLast(List<T> dataList){
        if(dataList != null && dataList.size() > 0){
            this.dataList.addAll(dataList);
        }
    }

    /**
     * Add the items to the current list on first
     * @param dataList
     */
    public void addItemsOnFirst(List<T> dataList){
        if(dataList != null && dataList.size() > 0){
            this.dataList.addAll(0, dataList);
        }
    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }
}
