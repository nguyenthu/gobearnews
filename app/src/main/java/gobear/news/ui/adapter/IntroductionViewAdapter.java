package gobear.news.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gobear.news.R;
import gobear.news.ui.adapter.base.BaseViewAdapter;
import gobear.news.ui.viewholder.base.BaseViewHolder;
import gobear.news.ui.viewholder.base.IntroductionViewHolder;

public class IntroductionViewAdapter extends BaseViewAdapter {
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ctrl_introduction_item, parent, false);
        return new IntroductionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bind(getItem(position));
    }
}
