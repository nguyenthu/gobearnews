package gobear.news.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gobear.news.R;
import gobear.news.data.model.NewsItemData;
import gobear.news.ui.adapter.base.BaseViewAdapter;
import gobear.news.ui.viewholder.base.BaseViewHolder;
import gobear.news.ui.viewholder.base.IntroductionViewHolder;
import gobear.news.ui.viewholder.base.NewsViewHolder;

public class NewsViewAdapter extends BaseViewAdapter {
    NewsViewHolder.OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(NewsViewHolder.OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ctrl_news_item, parent, false);
        NewsViewHolder newsViewHolder = new NewsViewHolder(itemView);
        newsViewHolder.setOnItemClickListener(onItemClickListener);
        return newsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bind(getItem(position));

    }
}
