package gobear.news.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import gobear.news.R;
import gobear.news.data.db.AppDbHelper;
import gobear.news.data.db.prefs.AppPreferencesHelper;
import gobear.news.data.model.NewsItemData;
import gobear.news.ui.activity.base.BaseActivity;
import gobear.news.ui.adapter.NewsViewAdapter;
import gobear.news.ui.viewholder.base.NewsViewHolder;
import gobear.news.util.DialogUtil;
import gobear.news.util.ProgressDialogUtil;
import gobear.news.viewmodel.NewsListViewModel;

public class NewsListActivity extends BaseActivity {
    @BindView(R.id.tvLogout)
    TextView tvLogout;
    @BindView(R.id.rvNews)
    RecyclerView rvNews;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView((R.id.tvNoNewsFound))
    View tvNoNewsFound;

    @Inject
    NewsListViewModel viewModel;

    @Inject
    AppPreferencesHelper appPreferencesHelper;

    boolean requestRefresh;

    @Override
    protected int layoutRes() {
        return R.layout.activity_news_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);

        tvLogout.setPaintFlags(tvLogout.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        // Attach adapter to recylerview
        final NewsViewAdapter adapter = new NewsViewAdapter();
        rvNews.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvNews.setAdapter(adapter);

        adapter.setOnItemClickListener(new NewsViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(NewsItemData newsItemData, View clickView, boolean clickOnImage) {
                // Go to news detail screen
                Intent intent = new Intent(NewsListActivity.this, NewsDetailActivity.class);
                // Pass data object in the bundle and populate details activity.
                intent.putExtra(NewsDetailActivity.NEWS_ITEM_DATA, newsItemData);
                if(clickOnImage) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(NewsListActivity.this, clickView, getString(R.string.image_transition_name));
                    startActivity(intent, options.toBundle());
                }else{
                    startActivity(intent);
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestRefresh = true;
                viewModel.fetchLatestNews();
            }
        });
        viewModel.getNews().observe(this, newsItemDataList -> {
            swipeRefreshLayout.setRefreshing(false);
            if(newsItemDataList != null && newsItemDataList.size() > 0) {
                rvNews.setVisibility(View.VISIBLE);
                tvNoNewsFound.setVisibility(View.GONE);
                // Update on UI
                adapter.setDataList(newsItemDataList);
                adapter.notifyDataSetChanged();
            }else{
                tvNoNewsFound.setVisibility(View.VISIBLE);
                rvNews.setVisibility(View.GONE);
            }
        });
        viewModel.getLoading().observe(this, loading ->{
            if(loading){
                if(!requestRefresh) {
                    ProgressDialogUtil.showLoading(NewsListActivity.this, getString(R.string.loading_news_text));
                }
            }else{
                ProgressDialogUtil.hideLoading();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        // Get news data
        viewModel.fetchNews();
    }

    @OnClick(R.id.tvLogout)
    public void logout(){
        DialogUtil.showConfirmDialog(this, getString(R.string.logout_confirm_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        appPreferencesHelper.turnLogout();

                        // Go to Login screen then finish
                        Intent intent = new Intent(NewsListActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, null);
    }
}
