package gobear.news.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import gobear.news.R;
import gobear.news.data.model.NewsItemData;
import gobear.news.ui.activity.base.BaseActivity;
import gobear.news.util.CommonUtil;
import gobear.news.util.Constant;
import gobear.news.util.DateUtil;

public class NewsDetailActivity extends BaseActivity {
    public static final String NEWS_ITEM_DATA = "news_item_data";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivNewsImage)
    ImageView ivNewsImage;
    @BindView(R.id.tvNewsTitle)
    TextView tvNewsTitle;
    @BindView(R.id.tvNewsDate)
    TextView tvNewsDate;
    @BindView(R.id.tvNewsStory)
    TextView tvNewsStory;

    @Override
    protected int layoutRes() {
        return R.layout.activity_news_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Retrieve data and show on UI
        NewsItemData newsItemData = getIntent().getParcelableExtra(NEWS_ITEM_DATA);
        if(newsItemData != null){
            getSupportActionBar().setTitle( newsItemData.getTitle());
            int screenWidth = CommonUtil.getScreenWidth(this);
            Glide
                    .with(this)
                    .load(newsItemData.getImageUrl())
                    .override(screenWidth, screenWidth/2)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_image_holder)
                    .into(ivNewsImage);

            tvNewsTitle.setText(newsItemData.getTitle());
            tvNewsDate.setText(DateUtil.displayDateFormat(Constant.FROM_DATE_FORMAT, newsItemData.getDateWithoutGMTText(), Constant.TO_DATE_FORMAT));
            tvNewsStory.setText(newsItemData.getDescription());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
