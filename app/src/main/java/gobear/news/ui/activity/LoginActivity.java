package gobear.news.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.EditText;
import android.widget.Switch;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import gobear.news.R;
import gobear.news.data.db.prefs.AppPreferencesHelper;
import gobear.news.ui.activity.base.BaseActivity;
import gobear.news.util.DialogUtil;

public class LoginActivity extends BaseActivity {

    static final String USER_NAME = "GoBear";
    static final String PASSWORD = "GoBearDemo";

    @BindView(R.id.etUserName)
    EditText etUserName;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.swRememberMe)
    SwitchCompat swRememberMe;

    @Inject
    AppPreferencesHelper appPreferencesHelper;

    @Override
    protected int layoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(appPreferencesHelper.isSaveInfo()){ // Fulfill the username and password text
            etUserName.setText(appPreferencesHelper.getUserName());
            etUserName.setSelection(appPreferencesHelper.getUserName().length());
            etPassword.setText(appPreferencesHelper.getPassword());
            swRememberMe.setChecked(true);
        }
    }

    @OnClick(R.id.btnLogin)
    public void clickLogin(){
        String userName = etUserName.getText().toString();
        String password = etPassword.getText().toString();

        if(USER_NAME.equalsIgnoreCase(userName) && PASSWORD.equalsIgnoreCase(password)){ // Login succeed
            // Save login info
            appPreferencesHelper.saveLoginInfo(userName, password, swRememberMe.isChecked());

            // Go to news list activity
            Intent intent = new Intent(this, NewsListActivity.class);
            startActivity(intent);

            finish();
        }else{
            DialogUtil.showInformDialog(this, getString(R.string.login_failed_text), null);
        }
    }
}
