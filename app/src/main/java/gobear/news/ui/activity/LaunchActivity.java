package gobear.news.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import gobear.news.R;
import gobear.news.data.db.prefs.AppPreferencesHelper;
import gobear.news.ui.activity.base.BaseActivity;
import gobear.news.ui.adapter.IntroductionViewAdapter;
import gobear.news.ui.widget.carousel.CircleIndicator;

public class LaunchActivity extends BaseActivity {
    @BindView(R.id.rvIntroduction)
    RecyclerView rvIntroduction;
    @BindView(R.id.imageCarousel)
    CircleIndicator imageCarousel;

    @Inject
    AppPreferencesHelper appPreferencesHelper;

    @Override
    protected int layoutRes() {
        return R.layout.activity_launch;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check login state and introduction state
        if(appPreferencesHelper.isSkipIntroduction()){
            if(appPreferencesHelper.isLogin()){
                // Go to News list screen
                Intent intent = new Intent(this, NewsListActivity.class);
                startActivity(intent);
            }else{
                // Go to Login screen
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            }
            finish();
        }else {
            rvIntroduction.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

            // Init view adapter
            IntroductionViewAdapter viewAdapter = new IntroductionViewAdapter();
            viewAdapter.setDataList(Arrays.asList(new String[]{
                    getString(R.string.first_introduction_text),
                    getString(R.string.second_introduction_text),
                    getString(R.string.third_introduction_text)}));
            rvIntroduction.setAdapter(viewAdapter);

            // Create pager adapter
            PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
            pagerSnapHelper.attachToRecyclerView(rvIntroduction);

            // Also attach recyclerview and pagerSnap to circle indicator
            imageCarousel.attachToRecyclerView(rvIntroduction, pagerSnapHelper);
        }
    }

    @OnClick(R.id.tvSkip)
    public void skip(){
        // Skip introduction
        appPreferencesHelper.skipIntroduction();

        // Go to Login screen
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();
    }
}
