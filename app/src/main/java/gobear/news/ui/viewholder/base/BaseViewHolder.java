package gobear.news.ui.viewholder.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(T itemData){

    }

    public T getDataItem() {
        return null;
    }

    public void setDataItem(T dataItem){
    }
}
