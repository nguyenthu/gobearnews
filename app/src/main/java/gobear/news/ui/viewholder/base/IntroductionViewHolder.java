package gobear.news.ui.viewholder.base;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import gobear.news.R;

public class IntroductionViewHolder extends BaseViewHolder<String> {
    @BindView(R.id.tvIntroduction)
    TextView tvIntroduction;

    public IntroductionViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(String itemData) {
        tvIntroduction.setText(itemData);
    }
}
