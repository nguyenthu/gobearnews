package gobear.news.ui.viewholder.base;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import gobear.news.R;
import gobear.news.data.model.NewsItemData;
import gobear.news.util.Constant;
import gobear.news.util.DateUtil;

public class NewsViewHolder extends BaseViewHolder<NewsItemData> {
    public interface OnItemClickListener{
        void onItemClick(NewsItemData newsItemData, View onClickView, boolean clickOnImage);
    }

    @BindView(R.id.ivNewsImage)
    ImageView ivNewsImage;
    @BindView(R.id.tvNewsTitle)
    TextView tvNewsTitle;
    @BindView(R.id.tvNewsDate)
    TextView tvNewsDate;
    @BindView(R.id.tvNewsStory)
    TextView tvNewsStory;

    OnItemClickListener onItemClickListener;
    NewsItemData newsItemData;

    public NewsViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(newsItemData, itemView, false);
                }
            }
        });
        ivNewsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(newsItemData, ivNewsImage,true);
                }
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void bind(NewsItemData itemData) {
        this.newsItemData = itemData;
        int scaleImageSize = itemView.getResources().getDimensionPixelSize(R.dimen.small_image_size);
        Glide
                .with(itemView.getContext())
                .load(itemData.getImageUrl())
                .override(scaleImageSize, scaleImageSize)
                .centerCrop()
                .placeholder(R.mipmap.ic_image_holder)
                .into(ivNewsImage);

        tvNewsTitle.setText(itemData.getTitle());
        tvNewsDate.setText(DateUtil.displayDateFormat(Constant.FROM_DATE_FORMAT, itemData.getDateWithoutGMTText(), Constant.TO_DATE_FORMAT));
        tvNewsStory.setText(itemData.getDescription());
    }

}
