package gobear.news.data.rest;

import java.util.List;

import gobear.news.data.model.ChannelItemData;
import gobear.news.data.model.NewsItemData;
import gobear.news.data.model.RssItemData;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface NewsService {
    @Headers({"Accept: application/xml"})
    @GET("/news/world/asia/rss.xml")
    Single<RssItemData> getNewsList();
}
