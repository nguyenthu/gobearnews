package gobear.news.data.rest;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import gobear.news.data.model.ChannelItemData;
import gobear.news.data.model.NewsItemData;
import gobear.news.data.model.RssItemData;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class NewsRepository {
    final NewsService newsService;

    @Inject
    public NewsRepository(NewsService newsService){ this.newsService = newsService;}

    public Single<RssItemData> getNewsList(){
        return newsService.getNewsList();
    }
}
