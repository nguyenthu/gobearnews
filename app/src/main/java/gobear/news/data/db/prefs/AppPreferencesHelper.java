package gobear.news.data.db.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import gobear.news.GoBearApplication;
import gobear.news.R;
import gobear.news.di.PreferenceInfo;

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {
    static final String USER_NAME = "user_name";
    static final String PASSWORD = "password";
    static final String SAVE_INFO = "save_info";
    static final String SKIP_INTRODUCTION = "skip_introduction";
    static final String LOGIN_STATE = "login_state";

    private final SharedPreferences mPrefs;
    private final SharedPreferences.Editor editor;

    @Inject
    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = mPrefs.edit();
    }

    public String getUserName(){
        return mPrefs.getString(USER_NAME, "");
    }
    public String getPassword(){
        return mPrefs.getString(PASSWORD, "");
    }
    public boolean isSaveInfo(){
        return mPrefs.getBoolean(SAVE_INFO, false);
    }
    public void saveLoginInfo(String userName, String password, boolean saveInfo){
        editor.putString(USER_NAME, userName);
        editor.putString(PASSWORD, password);
        editor.putBoolean(SAVE_INFO, saveInfo);
        editor.putBoolean(LOGIN_STATE, true);
        editor.commit();
    }
    public void clearUserInfoIfAny(){
        boolean isSaveInfo = isSaveInfo();
        if(!isSaveInfo){ // Clear user and password if user didn't want to save
            editor.remove(USER_NAME);
            editor.remove(PASSWORD);
            editor.commit();
        }
    }
    public void skipIntroduction(){
        editor.putBoolean(SKIP_INTRODUCTION, true);
        editor.commit();
    }
    public boolean isSkipIntroduction(){
        return mPrefs.getBoolean(SKIP_INTRODUCTION, false);
    }
    public void turnLogin(){
        editor.putBoolean(LOGIN_STATE, true);
        editor.commit();
    }
    public void turnLogout(){
        editor.putBoolean(LOGIN_STATE, false);
        editor.commit();
    }
    public boolean isLogin(){
        return mPrefs.getBoolean(LOGIN_STATE, false);
    }
}
