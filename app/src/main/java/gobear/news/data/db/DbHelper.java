package gobear.news.data.db;

import java.util.List;

import gobear.news.data.model.NewsItemData;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface DbHelper {


    Single<List<NewsItemData>> getAllNews();

    Single<Long[]> insertAllNews(List<NewsItemData> newsItemDataList);

    Single<Integer> deleteAllNews();

}
