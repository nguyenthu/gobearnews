package gobear.news.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import gobear.news.data.model.ImageUrlConverter;
import gobear.news.data.db.dao.NewsDao;
import gobear.news.data.model.NewsItemData;

@Database(entities = {NewsItemData.class}, version = 1)
@TypeConverters({ImageUrlConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDao newsDao();
}
