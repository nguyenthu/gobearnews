package gobear.news.data.db;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import gobear.news.data.model.NewsItemData;
import io.reactivex.Observable;
import io.reactivex.Single;

@Singleton
public class AppDbHelper implements DbHelper {

    private final AppDatabase mAppDatabase;

    @Inject
    public AppDbHelper(AppDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }

    @Override
    public Single<List<NewsItemData>> getAllNews() {
        return Single.fromCallable(new Callable<List<NewsItemData>>() {
            @Override
            public List<NewsItemData> call() throws Exception {
                return mAppDatabase.newsDao().loadAll();
            }
        });
    }

    @Override
    public Single<Long[]> insertAllNews(List<NewsItemData> newsItemDataList) {
        return Single.fromCallable(new Callable<Long[]>() {
            @Override
            public Long[] call() throws Exception {
                return mAppDatabase.newsDao().insertAll(newsItemDataList);
            }
        });
    }

    @Override
    public Single<Integer> deleteAllNews() {
        return Single.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return mAppDatabase.newsDao().deleteAll();
            }
        });
    }


}
