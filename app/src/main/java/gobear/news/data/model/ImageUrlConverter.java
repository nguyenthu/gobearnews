package gobear.news.data.model;

import android.arch.persistence.room.TypeConverter;

/**
 * Created by ThuNguyen on 3/10/19.
 */

public class ImageUrlConverter {
    @TypeConverter
    public static NewsItemData.Thumbnail fromString(String url){
        NewsItemData.Thumbnail thumbnail = new NewsItemData.Thumbnail();
        thumbnail.setUrl(url);
        return thumbnail;
    }
    @TypeConverter
    public static String fromMediaThumbnail(NewsItemData.Thumbnail thumbnail){
        return thumbnail.getUrl();
    }
}
