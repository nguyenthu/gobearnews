package gobear.news.data.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by ThuNguyen on 3/9/19.
 */
@Root(name = "channel", strict = false)
public class ChannelItemData {
    @ElementList(name = "item", inline = true, entry = "item")
    ArrayList<NewsItemData> newsItemDataArrayList;

    public ArrayList<NewsItemData> getNewsItemDataArrayList() {
        return newsItemDataArrayList;
    }

}
