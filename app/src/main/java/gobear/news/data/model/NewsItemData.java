package gobear.news.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Entity(tableName = "news")
@Root(name = "item", strict = false)
public class NewsItemData implements Parcelable{

    @PrimaryKey
    @NonNull
    @Element(name = "pubDate")
    public String date;

    @Element(name = "title")
    public String title;

    @ColumnInfo(name = "image_url")
    @Element(name = "thumbnail", required = false)
    public Thumbnail thumbnail;

    @Element(name = "description")
    public String description;

    public NewsItemData(){}
    protected NewsItemData(Parcel in) {
        date = in.readString();
        title = in.readString();
        thumbnail = in.readParcelable(Thumbnail.class.getClassLoader());
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(title);
        dest.writeParcelable(thumbnail, flags);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewsItemData> CREATOR = new Creator<NewsItemData>() {
        @Override
        public NewsItemData createFromParcel(Parcel in) {
            return new NewsItemData(in);
        }

        @Override
        public NewsItemData[] newArray(int size) {
            return new NewsItemData[size];
        }
    };

    public String getDate() {
        return date;
    }
    public String getDateWithoutGMTText(){
        if(date != null && date.contains("GMT")){
            return date.replace("GMT", "").trim();
        }
        return date;
    }
    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return thumbnail != null ? thumbnail.getUrl() : null;
    }

    public String getDescription() {
        return description;
    }


    @Root(strict = false)
    public static class Thumbnail implements Parcelable{
        @Attribute(name = "url")
        String url;

        public Thumbnail(){}
        protected Thumbnail(Parcel in) {
            url = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(url);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Thumbnail> CREATOR = new Creator<Thumbnail>() {
            @Override
            public Thumbnail createFromParcel(Parcel in) {
                return new Thumbnail(in);
            }

            @Override
            public Thumbnail[] newArray(int size) {
                return new Thumbnail[size];
            }
        };

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }
}
