package gobear.news.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by ThuNguyen on 3/10/19.
 */
@Root(name = "rss", strict = false)
public class RssItemData {

    @Element(name = "channel")
    ChannelItemData channelItemData;


    public ChannelItemData getChannelItemData() {
        return channelItemData;
    }
}
