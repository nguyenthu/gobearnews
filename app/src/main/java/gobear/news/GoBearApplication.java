package gobear.news;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import gobear.news.di.component.ApplicationComponent;
import gobear.news.di.component.DaggerApplicationComponent;
import gobear.news.di.module.RoomModule;

public class GoBearApplication extends DaggerApplication {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).roomModule(new RoomModule(this)).build();
        component.inject(this);
        return component;
    }
}
