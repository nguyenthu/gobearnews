package gobear.news.di.module;

import android.arch.lifecycle.ViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import gobear.news.di.util.ViewModelKey;
import gobear.news.viewmodel.NewsDetailViewModel;
import gobear.news.viewmodel.NewsListViewModel;

@Singleton
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(NewsListViewModel.class)
    abstract ViewModel bindListViewModel(NewsListViewModel listViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NewsDetailViewModel.class)
    abstract ViewModel bindDetailsViewModel(NewsDetailViewModel detailsViewModel);

}
