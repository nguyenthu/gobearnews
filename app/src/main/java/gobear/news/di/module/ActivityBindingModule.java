package gobear.news.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import gobear.news.ui.activity.LaunchActivity;
import gobear.news.ui.activity.LoginActivity;
import gobear.news.ui.activity.NewsDetailActivity;
import gobear.news.ui.activity.NewsListActivity;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract LaunchActivity bindLaunchActivity();

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector
    abstract NewsListActivity bindNewsListListActivity();

    @ContributesAndroidInjector
    abstract NewsDetailActivity bindNewsDetailActivity();

}
