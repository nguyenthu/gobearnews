package gobear.news.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gobear.news.data.db.AppDatabase;
import gobear.news.data.db.dao.NewsDao;

/**
 * Created by ThuNguyen on 3/9/19.
 */

@Module
public class RoomModule {

    private AppDatabase appDatabase;

    public RoomModule(Application mApplication) {
        appDatabase = Room.databaseBuilder(mApplication, AppDatabase.class, "gobear-db").build();
    }

    @Singleton
    @Provides
    AppDatabase providesRoomDatabase() {
        return appDatabase;
    }

    @Singleton
    @Provides
    NewsDao providesNewsDao(AppDatabase appDatabase) {
        return appDatabase.newsDao();
    }

}
