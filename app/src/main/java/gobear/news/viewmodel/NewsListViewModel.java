package gobear.news.viewmodel;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import gobear.news.data.db.AppDbHelper;
import gobear.news.data.model.ChannelItemData;
import gobear.news.data.model.NewsItemData;
import gobear.news.data.model.RssItemData;
import gobear.news.data.rest.NewsRepository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class NewsListViewModel extends ViewModel{
    private final NewsRepository newsRepository;
    private CompositeDisposable disposable;

    private final MutableLiveData<List<NewsItemData>> news = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();
    AppDbHelper appDbHelper;

    @Inject
    public NewsListViewModel(NewsRepository newsRepository, AppDbHelper appDbHelper) {
        this.newsRepository = newsRepository;
        this.appDbHelper = appDbHelper;
        disposable = new CompositeDisposable();
        fetchLatestNews();
    }

    public LiveData<List<NewsItemData>> getNews() {
        return news;
    }
    LiveData<Boolean> getError() {
        return repoLoadError;
    }
    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public void fetchLatestNews() {
        loading.setValue(true);
        disposable.add(newsRepository.getNewsList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<RssItemData>() {
                    @Override
                    public void onSuccess(RssItemData value) {
                        repoLoadError.setValue(false);
                        news.setValue(value.getChannelItemData().getNewsItemDataArrayList());
                        loading.setValue(false);

                        updateDataToLocal(value.getChannelItemData().getNewsItemDataArrayList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                    }
                }));
    }
    public void fetchNews(){
        loading.setValue(true);
        disposable.add(appDbHelper.getAllNews().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<NewsItemData>>() {
                    @Override
                    public void onSuccess(List<NewsItemData> value) {
                        if(value == null || value.size() == 0){
                            // Fetch latest news
                            fetchLatestNews();
                        }else{
                            repoLoadError.setValue(false);
                            news.setValue(value);
                            loading.setValue(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Fetch latest news
                        fetchLatestNews();
                    }
                }));
    }
    private void updateDataToLocal(List<NewsItemData> itemDataList){
        disposable.add(appDbHelper.deleteAllNews()
                .subscribeOn(Schedulers.io())
                .flatMap(integer -> appDbHelper.insertAllNews(itemDataList))
                .subscribeWith(new DisposableSingleObserver<Long[]>() {
                    @Override
                    public void onSuccess(Long[] o) {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }
    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
