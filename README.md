# GoBearNews guideline

1. Technical overview
- Architecture: MVVM
- Dependency Injection : Dagger 2
- Multi-thread: RxJava
- Networking: Retrofit 2
- Local storage: Room database
- Parser: Simple Xml Parser
- Data observer: LiveData
- Image loader: Glide

2. Config/Build/Deploy
- All library versions will be defined in "ext" of build.gradle file at project level.
It helps change the latest versions easily.
- Add this project and the IDE itself automatically build
- Due to applying Dagger 2, the project would show the error on DaggerApplicationComponent of GoBearApplication after
ending up sync. Fortunately, press RUN the app anyway then the error would dismiss and install the app to device afterward.

3. Flow of the app
3.1 Launch screen
- Technical:
 + RecylerView with SnapPager.
 + Circle Indicator custom widget.
- Show the screen with 3 sliding introduction pages.
- User presses "Skip" button to dismiss and navigate to Login screen if user hasn't log gin before
or to News List screen otherwise.
- This screen only shows once in whole lifecycle as users click the "Skip" button.

3.2 Login screen
- Technical:
  + SharedPreference to save login info
- Pre-fill username, password or Remember me if user already save this info before

3.3 News List screen
- Technical:
  + RecyclerView
  + Glide for image loader
  + Load News data from local (Room database)
- Load data from local first. If the return data was not empty, then get data from api
and store the latest news to local.
- "Refresh" will always get newest data from api.
- Click any news item to navigate to News detail screen
- Click logout will reset login info state and back to Login screen.
- Apply element transition to transit the image of list to detail to make app more vividly.

3.4 News Detail screen
- Receive the News parcelable from News Screen and show it on UI
- Click back to reset effect transition
